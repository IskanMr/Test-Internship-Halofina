//Jawaban soal 1
//Melakukan for loop console.log nilai k dari k=1 hingga k=50
for (let k = 1; k <= 50; k++) {
  console.log(k);
}

//Jawaban soal 2
//Pengujian Number
var Number = 6;

if (Number == 1) {
  //Output saat Number = 1
  console.log("Number is 1");
} else if (Number == 6) {
  //Output saat Number = 6
  console.log("Number is 6");
} else {
  //Output saat Number selain 1 dan 6
  console.log("All other numbers");
}

//Jawaban soal 3
//Menerima masukan nilai alas dan tinggi
function triangleArea(alas, tinggi) {
  //Me-return nilai dari luas segitiga berdasarkan masukan
  return 0.5 * alas * tinggi;
}
//Pengujian fungsi triangleArea
console.log(triangleArea(3, 4));

//Jawaban soal 4
//Menerima masukan nilai a dan b
function Max2(a, b) {
  //Me-return nilai tertinggi dari kedua masukan
  return a > b ? a : b;
}

//Menerima masukan nilai a, b, dan c
function Max3(a, b, c) {
  //Me-return nilai tertinggi dari nilai tertinggi dari a dan b dan nilai tertinggi dari b dan c
  //Nilai tertinggi dari ketiga masukan
  return Max2(Max2(a, b), Max2(b, c));
}

//Menerima masukan nilai a, b, c, d, e, f, g, h, dan i
function Max9(a, b, c, d, e, f, g, h, i) {
  //Mer-return nilai tertinggi dari nilai tertinggi dari a, b, dan c dengan nilai tertinggi dari d, e, dan f
  //dengan nilai tertinggi dari g, h, dan i
  //Nilai tertinggi dari kesembilan masukan
  return Max3(Max3(a, b, c), Max3(d, e, f), Max3(g, h, i));
}

//Pengujian fungsi Max9
console.log(Max9(29, 32, 19, 1, 2, 67, 12, 49, 23));

//Jawaban soal 5
//Mendefinisikan array nilai awal dan negara
var Nilai = [0, 0, 0, 0];
var Countries = ["Indonesia", "Malaysia", "Jepang", "Cina"];

//Membuat variabel dari nama negara
const Indonesia = 0;
const Malaysia = 1;
const Jepang = 2;
const Cina = 3;

//Array nama team1 dan team2
var NamaTeam1 = [
  "Indonesia",
  "Malaysia",
  "Cina",
  "Cina",
  "Indonesia",
  "Jepang",
];
var NamaTeam2 = [
  "Malaysia",
  "Jepang",
  "Malaysia",
  "Indonesia",
  "Jepang",
  "Cina",
];

//Array skor team1 dan team2
var SkorTeam1 = [5, 1, 3, 0, 0, 1];
var SkorTeam2 = [2, 3, 0, 2, 0, 3];

//For loop untuk menghitung nilai negara selama pertandingan
for (let id = 0; id < SkorTeam1.length; id++) {
  //Mengubah string nama team menjadi variabel
  const id1 = eval(NamaTeam1[id]);
  const id2 = eval(NamaTeam2[id]);

  if (SkorTeam1[id] > SkorTeam2[id]) {
    //Nilai team1 ditambah 3 jika menang
    Nilai[id1] += 3;
  } else if (SkorTeam1[id] == SkorTeam2[id]) {
    //Nilai team1 dan 2 ditambah 1 jika seri
    Nilai[id1]++;
    Nilai[id2]++;
  } else if (SkorTeam2[id] > SkorTeam1[id]) {
    //Nilai team2 ditambah 3 jika menang
    Nilai[id2] += 3;
  }
}

//For loop untuk menampilkan nama negara dan nilai akhirnya
for (let i = 0; i < Countries.length; i++) {
  console.log(Countries[i], " : ", Nilai[i]);
}

//Jawaban Soal 5
//Array text yang akan digunakan untuk test
const testTexts1 = [
  "haveaniceday",
  "feedthedog",
  "chillout",
  "if man was meant to stay on the ground god would have given us roots",
];

//Jika ingin mengambil text dari pengguna
//   let texts1 = ("Masukan Kalimat Anda untuk dienkripsi: ")
//     .split(" ")
//     .join("");

//Fungsi enskripsi dengan masukan text(string)
function toEncrypt(text) {
  //menghilangkan spasi di antara kata
  const newText = text.split(" ").join("");

  //Mendefinisikan jumlah huruf text
  const textLength = newText.length;

  //Mendefinisikan baris square code
  var baris = Math.round(Math.sqrt(textLength));
  var kolom = 0;

  //Menentukan kolom square code
  if (baris >= Math.sqrt(textLength)) {
    kolom = baris;
  } else {
    kolom = baris + 1;
  }

  //Mendefinisikan variabel text yang telah terenkripsi
  var encryptedText = "";

  //Dua for loop untuk mengisi text yang telah terenkripsi
  //Dimasukkan huruf dari urutan dari kiri atas kotak ke kanan bawah
  for (let i = 0; i < kolom; ++i) {
    for (let j = i; j < textLength; j += kolom) {
      encryptedText += newText[j];
    }
    //Memberikan spasi diantara kolom
    encryptedText += " ";
  }

  //Me-return text yang telah terenkripsi
  return encryptedText;
}

//Menampilkan test texts yang telah terenkripsi
for (let i = 0; i < testTexts1.length; i++) {
  console.log(toEncrypt(testTexts1[i]));
}

//Jawaban soal bonus
//Array text yang akan digunakan untuk test
const testTexts2 = [
  "hae and via ecy",
  "fto ehg ee dd",
  "clu hlt io",
  "imtgdvs fearwer mayoogo anouuio ntnnlvt wttddes aohghn sseoau",
];

//Jika ingin mengambil text dari pengguna
//   let texts2 = prompt("Masukan Kalimat Anda untuk didekripsi: ")
//     .split(" ")
//     .join("");

//Fungsi untuk mendeskripsi
function toDecrypt(text) {
  //Menghilangkan spasi di antara kata
  const newText = text.split(" ").join("");

  //Mendefinisikan array kata dari text
  var words = text.split(/(\s+)/).filter((e) => e.trim().length > 0);

  //Mendefinisikan jumlah huruf di text
  const textLength = newText.length;

  //Mendefinisikan baris
  var baris = Math.round(Math.sqrt(textLength));
  var kolom = 0;

  //Menentukan kolom
  if (baris >= Math.sqrt(textLength)) {
    kolom = baris;
  } else {
    kolom = baris + 1;
  }

  //Mendefinisikan variabel text yang telah dideskripsi
  var decryptedText = "";

  //Dua for loop mengisikan variabel text yang telah dideskripsi
  //Menambahkan huruf sesuai index kolom dan baris
  for (let i = 0; i < baris; ++i) {
    for (let j = 0; j < kolom; ++j) {
      //Hanya menambahkan string
      if (typeof words[j][i] === "string") {
        decryptedText += words[j][i];
      }
    }
  }

  //Me-return text yang telah dideskripsi
  return decryptedText;
}

//For loop untuk menampilkan text yang telah dideskripsi
for (let i = 0; i < testTexts2.length; i++) {
  console.log(toDecrypt(testTexts2[i]));
}
